package com.example.springsecurity.security.filter;

import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @类名: SecurityFilter
 * @描述: TODO
 * @作者: mengh
 * @创建时间: 2020/11/18
 * @版本: V1.0
 **/
//public class SecurityFilter extends AbstractAuthenticationProcessingFilter {
public class SecurityFilter{

    /**
     * 调用 #requiresAuthentication(HttpServletRequest, HttpServletResponse) 决定是否需要进行验证操作。
     * 如果需要验证，则会调用 #attemptAuthentication(HttpServletRequest, HttpServletResponse) 方法。
     * 有三种结果：
     * 1、返回一个 Authentication 对象。
     * 配置的 SessionAuthenticationStrategy` 将被调用，
     * 然后 然后调用 #successfulAuthentication(HttpServletRequest，HttpServletResponse，FilterChain，Authentication) 方法。
     * 2、验证时发生 AuthenticationException。
     * #unsuccessfulAuthentication(HttpServletRequest, HttpServletResponse, AuthenticationException) 方法将被调用。
     * 3、返回Null，表示身份验证不完整。假设子类做了一些必要的工作（如重定向）来继续处理验证，方法将立即返回。
     * 假设后一个请求将被这种方法接收，其中返回的Authentication对象不为空。
     */
//    @Override
//    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
//        HttpServletRequest request =  (HttpServletRequest)req;
//        HttpServletResponse response =  (HttpServletResponse)res;
//        boolean requiresAuthentication = requiresAuthentication(request, response);
//        if(!requiresAuthentication){
//            chain.doFilter(request, response);
//            return;
//        }
//        Authentication authResult;
//        try {
//            authResult = attemptAuthentication(request, response);
//            if (authResult == null) {
//                // return immediately as subclass has indicated that it hasn't completed
//                // authentication
//                return;
//            }
//            sessionStrategy.onAuthentication(authResult, request, response);
//        }
//        catch (InternalAuthenticationServiceException failed) {
//            logger.error(
//                    "An internal error occurred while trying to authenticate the user.",
//                    failed);
//            unsuccessfulAuthentication(request, response, failed);
//
//            return;
//        }
//        catch (AuthenticationException failed) {
//            // Authentication failed
//            unsuccessfulAuthentication(request, response, failed);
//
//            return;
//        }
//
//        // Authentication success
//        if (continueChainBeforeSuccessfulAuthentication) {
//            chain.doFilter(request, response);
//        }
//
//        successfulAuthentication(request, response, chain, authResult);
//    }

//    @Override
//    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException, ServletException {
//        if (postOnly && !request.getMethod().equals("POST")) {
//            throw new AuthenticationServiceException(
//                    "Authentication method not supported: " + request.getMethod());
//        }
//
//        String username = obtainUsername(request);
//        String password = obtainPassword(request);
//
//        if (username == null) {
//            username = "";
//        }
//
//        if (password == null) {
//            password = "";
//        }
//
//        username = username.trim();
//
//        UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(
//                username, password);
//
//        // Allow subclasses to set the "details" property
//        setDetails(request, authRequest);
//
//        return this.getAuthenticationManager().authenticate(authRequest);
//    }

}
