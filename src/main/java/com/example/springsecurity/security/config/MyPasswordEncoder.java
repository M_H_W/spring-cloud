package com.example.springsecurity.security.config;

import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @类名: MyPasswordEncoder
 * @描述: TODO
 * @作者: mengh
 * @创建时间: 2020/11/18
 * @版本: V1.0
 **/
public class MyPasswordEncoder implements PasswordEncoder {

    @Override
    public String encode(CharSequence charSequence) {
        return charSequence.toString();
    }

    @Override
    public boolean matches(CharSequence charSequence, String s) {
        return s.equals(charSequence.toString());
    }
}