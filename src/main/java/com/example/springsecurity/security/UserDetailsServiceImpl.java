package com.example.springsecurity.security;

import com.example.springsecurity.security.entity.User;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @类名: UserDetailsServiceImpl
 * @描述: TODO
 * @作者: mengh
 * @创建时间: 2020/11/18
 * @版本: V1.0
 **/
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
       String userName = "menghw";
       String password = "123456";
       if("".equals(userName) && !s.equals(userName)){
           throw new UsernameNotFoundException("用户不存在！！！");
       }
       List<SimpleGrantedAuthority> simpleGrantedAuthorities = new ArrayList<>();
       simpleGrantedAuthorities.add(new SimpleGrantedAuthority("USER"));
       return new org.springframework.security.core.userdetails.User(userName, password, simpleGrantedAuthorities);
    }

}
