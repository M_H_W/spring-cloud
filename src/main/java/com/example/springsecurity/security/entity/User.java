package com.example.springsecurity.security.entity;

import org.springframework.security.core.GrantedAuthority;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

/**
 * 用户
 *
 * @author chenwandong
 */
public class User implements LoginUser{

    /**
     * 用户状态可用A
     */
    public static final String USER_STATUS_ABLE = "A";
    /**
     * 用户状态不可用D
     */
    public static final String USER_STATUS_DISABLE = "D";
    private static final long serialVersionUID = -3513874558277546590L;
    private String deptId;
    private String deptName;
    private String loginName;
    private String loginPass;
    private String empCode;
    private String name;
    private String sex;
    private String telephone;
    private String mobile;
    private String email;
    private String userType;
    private String status;
    private String delFlag;
    private String remark;
    private Date pwdChangeTime;

    private String objectType;
    private Integer objectId;

    private Integer lang;
    private String signature;
    /**
     * 默认0,申请中1,已开通2
     */
    private Integer vpnState;
    private Integer vpnSendState;

    private String commendNotice;

    private String greenGuide;

    /**
     * 用户是否在线：0在线，1或者null：不在线
     * */
    private String isOnline;


    private Collection<GrantedAuthority> authorities = null;

    private String roleName;

    public static String getUserStatusAble() {
        return USER_STATUS_ABLE;
    }

    public static String getUserStatusDisable() {
        return USER_STATUS_DISABLE;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getDeptId() {
        return deptId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getLoginPass() {
        return loginPass;
    }

    public void setLoginPass(String loginPass) {
        this.loginPass = loginPass;
    }

    public String getEmpCode() {
        return empCode;
    }

    public void setEmpCode(String empCode) {
        this.empCode = empCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getPwdChangeTime() {
        return pwdChangeTime;
    }

    public void setPwdChangeTime(Date pwdChangeTime) {
        this.pwdChangeTime = pwdChangeTime;
    }

    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    public Integer getObjectId() {
        return objectId;
    }

    public void setObjectId(Integer objectId) {
        this.objectId = objectId;
    }

    public Integer getLang() {
        return lang;
    }

    public void setLang(Integer lang) {
        this.lang = lang;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public Integer getVpnState() {
        return vpnState;
    }

    public void setVpnState(Integer vpnState) {
        this.vpnState = vpnState;
    }

    public Integer getVpnSendState() {
        return vpnSendState;
    }

    public void setVpnSendState(Integer vpnSendState) {
        this.vpnSendState = vpnSendState;
    }

    public String getCommendNotice() {
        return commendNotice;
    }

    public void setCommendNotice(String commendNotice) {
        this.commendNotice = commendNotice;
    }

    public String getGreenGuide() {
        return greenGuide;
    }

    public void setGreenGuide(String greenGuide) {
        this.greenGuide = greenGuide;
    }

    public String getIsOnline() {
        return isOnline;
    }

    public void setIsOnline(String isOnline) {
        this.isOnline = isOnline;
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return null;
    }

    @Override
    public String getUsername() {
        return null;
    }

    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return false;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return false;
    }

    @Override
    public boolean isEnabled() {
        return false;
    }

    public void setAuthorities(Collection<GrantedAuthority> authorities) {
        this.authorities = authorities;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    @Override
    public Serializable getId() {
        return null;
    }
}