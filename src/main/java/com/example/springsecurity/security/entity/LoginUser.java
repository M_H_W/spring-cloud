package com.example.springsecurity.security.entity;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serializable;

public interface LoginUser extends UserDetails {
    static LoginUser getLoginUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication != null ? (LoginUser)authentication.getPrincipal() : null;
    }

    Serializable getId();
}
